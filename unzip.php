<?
/*
	Unzip archive.zip to current folder - Wordpress migration
	version	1.0
	author	Peter Cesnek
	date 	2016-01-29
*/
	
// assuming file.zip is in the same directory as the executing script.
$file = 'archive.zip';

// get the absolute path to $file
$path = pathinfo(realpath($file), PATHINFO_DIRNAME);

$zip = new ZipArchive;
$res = $zip->open($file);
if ($res === TRUE) {
  // extract it to the path we determined above
  $zip->extractTo($path);
  $zip->close();
  echo "WOOT! $file extracted to $path";
} else {
  echo "Doh! I couldn't open $file";
}
?>