<?php
/*
	Automatic Wordpress Deployment
	version	1.2
	author	Peter Cesnek
	date 	2015-08-24
*/

/***************
   DOWNLOAD
****************/
$file = "wordpress-deploy.zip";
if (file_put_contents($file, fopen("http://wp-deploy.cesnek.sk/".$file, 'r'))) {	echo "Download OK <br>"; }
else {
	echo "Download Error<br>";
	exit();
}

/***************
   PACKAGE
****************/
echo "Package..<br>";
$path = './'; $file = "wordpress-deploy.zip";
echo "Found " . $file . "";
$zip = new ZipArchive;
$res = $zip->open($file);
if ($res === TRUE) {
	$zip->extractTo($path);
	$zip->close();
	
	unlink($file);
}

function DeletePath($path) {
	if (is_dir($path) === true)
	{
		$files = array_diff(scandir($path), array('.', '..'));

		foreach ($files as $file)
		{
			DeletePath(realpath($path) . '/' . $file);
		}

		return rmdir($path);
	}

	else if (is_file($path) === true)
	{
		return unlink($path);
	}

	return false;
}

/***************
   WORDPRESS
****************/
foreach (glob("wordpress-4.*.zip") as $file) {
	echo "Found " . $file . "<br>";
}

// get the absolute path to $file
$path = pathinfo(realpath($file), PATHINFO_DIRNAME);

$zip = new ZipArchive;
$res = $zip->open($file);
if ($res === FALSE) {
	echo "Doh! I couldn't open $file";
	} else {
	// extract it to the path we determined above
	$zip->extractTo($path);
	$zip->close();
	echo "Wordpress $file was extracted to $path";

	// delete original file (for space saving)
	unlink($file);

	/***************
		  THEMES
	****************/
	echo "<br><br><b>Themes...</b><br>";
	$path = './wp-content/themes/';
	foreach (glob("./themes/*.zip") as $file) {
		echo "Found " . $file . "";
		$zip = new ZipArchive;
		$res = $zip->open($file);
		if ($res === TRUE) {
			$zip->extractTo($path);
			$zip->close();
			
			unlink($file);
		}
	}
	rmdir('themes');

	/***************
		  PLUGINS
	****************/
	echo "<br><br><b>Plugins...</b><br>";
	$path = './wp-content/plugins/';
	foreach (glob("./plugins/*.zip") as $file) {
		echo "Found " . $file . "<br>";
		$zip = new ZipArchive;
		$res = $zip->open($file);
		if ($res === TRUE) {
			$zip->extractTo($path);
			$zip->close();
			
			unlink($file);
		}
	}
	rmdir('plugins');
	echo '<br>';

	/***************
	  CUSTOMIZATION
	****************/
	//- footer.php @ Divi
	echo 'Divi footer.php <br>';
	$file = './deploy/footer.php';
	copy ($file,'./wp-content/themes/Divi/footer.php');
	unlink($file);
	rmdir('deploy');


	//- remove Hello plugins
	echo 'Plugin Hello <br>';
	$file = './wp-content/plugins/hello.php'; unlink($file);

	//-remove not used themes - wp-content\themes\twentysixteen
	echo 'Theme twentysixteen <br>';
	DeletePath('./wp-content/themes/twentysixteen');
	
	//-remove not used themes - wp-content\themes\twentyfifteen
	echo 'Theme twentyfifteen <br>';
	DeletePath('./wp-content/themes/twentyfifteen');

	//-remove not used themes - wp-content\themes\twentyfourteen
	echo 'Theme twentyfourteen <br>';
	DeletePath('./wp-content/themes/twentyfourteen');

	//-remove not used themes - wp-content\themes\twentythirteen
	/*	echo 'Theme twentythirteen <br>';
	DeletePath('./wp-content/themes/twentythirteen');	*/

	//-remove particular Files
	$file = 'readme.html'; echo $file.' <br>'; unlink('./'.$file);

	echo '<br><br><br>You will be redirected.. in 5 seconds';
	sleep(5);
	header('Location: index.php');
}	
?>